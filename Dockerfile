FROM alpine:3.15.0

RUN apk update
RUN apk add openrc
RUN apk add nginx
RUN adduser -D -g 'www' www
RUN mkdir /www
RUN chown -R www:www /var/lib/nginx
RUN chown -R www:www /www

# CMD ["rc-service", "nginx", "start"]
CMD ["nginx", "-g", "daemon off;"]

